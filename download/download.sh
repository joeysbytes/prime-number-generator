#!/usr/bin/env bash

BASE_URL="https://primes.utm.edu/lists/small/millions"
DOWNLOAD_DIR="/home/joey/Downloads/primes"


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    for i in {1..50}
    do
        download_prime_zip_file "${i}"
    done
}


function download_prime_zip_file() {
    local file_number
    file_number="${1}"
    local file_name="primes${file_number}.zip"
    local prime_url="${BASE_URL}/${file_name}"
    local output_file="${DOWNLOAD_DIR}/${file_name}"
    if [ -f "${output_file}" ]
    then
        echo "File exists: ${file_name}"
    else
        echo "Downloading file: ${file_name}"
        wget -O "${output_file}" "${prime_url}"
    fi
}


###########################################################################
# Begin Script
###########################################################################
main
