#!/usr/bin/env bash
set -e

DOWNLOAD_DIR="/home/joey/Downloads/primes"
TEMP_DIR="/tmp2/primes"
PRIMES_FILE="/tmp2/all_primes.txt"
SCRIPT_DIR=$(pwd)


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    # Remove prime number file if it already exists
    if [ -f "${PRIMES_FILE}" ]
    then
        rm "${PRIMES_FILE}"
        touch "${PRIMES_FILE}"
    fi

    # Build prime number file
    for i in {1..50}
    do
        extract_prime_zip_file "${i}"
    done
}


function extract_prime_zip_file() {
    local file_number
    file_number="${1}"
    local zip_file_name="primes${file_number}.zip"
    local extracted_file_name="primes${file_number}.txt"
    echo "Processing file: ${zip_file_name}"
    cd "${TEMP_DIR}"
    echo "  Uncompressing..."
    unzip -q -o "${DOWNLOAD_DIR}/${zip_file_name}"
    echo "  Extracting..."
    python3 "${SCRIPT_DIR}/extract.py" "${TEMP_DIR}/${extracted_file_name}" "${PRIMES_FILE}"
    echo "  Cleanup..."
    rm "${TEMP_DIR}/${extracted_file_name}"
}

###########################################################################
# Begin Script
###########################################################################
main
