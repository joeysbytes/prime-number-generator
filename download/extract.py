import sys

# 0A = unix newline
# 0D0A = dos newline

# All lines end in these 3 bytes
LINE_ENDING = "\x0D" + "\x0D" + "\x0A"

INPUT_FILE = None
OUTPUT_FILE = None


def main():
    # print(f"Input file: {INPUT_FILE}")
    # print(f"Output file: {OUTPUT_FILE}")
    first_line = True
    file_out = open(OUTPUT_FILE, 'a')
    with open(INPUT_FILE, 'r') as file_in:
        for line in file_in:
            data = line.strip(LINE_ENDING).strip()
            if len(data) > 0:
                if first_line:
                    first_line = False
                else:
                    extract_prime_numbers(data, file_out)
    file_out.close()


def extract_prime_numbers(data, file_out):
    numbers = data.split()
    for number in numbers:
        file_out.write(f"{number}\n")


if __name__ == "__main__":
    INPUT_FILE = sys.argv[1]
    OUTPUT_FILE = sys.argv[2]
    main()
