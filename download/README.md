# Downloading Prime Number Lists from the Internet

These scripts will download and sanitize the first 50 million prime numbers. This is useful for validating the prime
number generator results.

Source: [University of Tennessee Prime Number Lists](https://primes.utm.edu/lists/small/millions/)

Scripts:

* download.sh - downloads the prime number zip files
* extract.sh - retrieves the prime numbers from the zip files and creates 1 big prime number file
