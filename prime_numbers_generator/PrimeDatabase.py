import sqlite3


class PrimeDatabase:

    #######################################################################
    # Database Initialization / Properties
    #######################################################################

    def __init__(self, file_name):
        self._file_name = file_name
        self._is_open = False
        self._connection = None
        self._cursor = None

    def __str__(self):
        text = f"File Name: {self._file_name}"
        text += f", Open: {self._is_open}"
        return text

    @property
    def file_name(self) -> str:
        return self._file_name

    @property
    def is_open(self) -> bool:
        return self._is_open

    #######################################################################
    # Open / Close the Database
    #######################################################################

    def open(self) -> None:
        """Open the database, if not already open"""
        if not self.is_open:
            self._open_connection()
            self._get_cursor()
            self._is_open = True

    def _open_connection(self) -> None:
        """Open the database connection."""
        try:
            self._connection = sqlite3.connect(self.file_name, isolation_level=None)
        except sqlite3.Error as error:
            print(f"Error opening database: {self.file_name}")
            raise error

    def _get_cursor(self) -> None:
        """Get the database cursor from the connection."""
        try:
            self._cursor = self._connection.cursor()
        except sqlite3.Error as error:
            print(f"Error getting cursor")
            raise error

    def close(self) -> None:
        """Close the database, if it is open"""
        if self.is_open:
            self._close_cursor()
            self._close_connection()
            self._is_open = False

    def _close_cursor(self) -> None:
        """Close the connection cursor."""
        try:
            self._cursor.close()
        except sqlite3.Error as error:
            print("Error closing cursor")
            raise error

    def _close_connection(self) -> None:
        """Close the database connection."""
        try:
            self._connection.close()
        except sqlite3.Error as error:
            print("Error closing connection")
            raise error

    #######################################################################
    # Database SQL
    #######################################################################

    def create_tables(self) -> None:
        """Create the database tables."""
        description = "Create prime_numbers table"
        prime_numbers_table_sql = ("CREATE TABLE IF NOT EXISTS prime_numbers " +
                                   "(idx INT PRIMARY KEY, " +
                                   " prime INT UNIQUE) " +
                                   "WITHOUT ROWID, STRICT")
        self._execute_sql(prime_numbers_table_sql, description)
        self._commit(description)

    def _execute_sql(self, sql: str, description: str):
        """Execute a given SQL."""
        try:
            results = self._cursor.execute(sql)
            return results
        except sqlite3.Error as error:
            print(f"Error executing SQL for: {description}")
            raise error

    def _commit(self, description: str) -> None:
        """Perform a database commit."""
        try:
            self._connection.commit()
        except sqlite3.Error as error:
            print(f"Error executing a commit for: {description}")
            raise error
