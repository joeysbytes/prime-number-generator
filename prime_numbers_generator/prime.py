from PrimeDatabase import PrimeDatabase

PRIME_DB_FILE = ":memory:"
MAX_INT = (2 ** 63) - 1


def main():
    prime_db = PrimeDatabase(PRIME_DB_FILE)
    try:
        prime_db.open()
        prime_db.create_tables()
    finally:
        prime_db.close()


if __name__ == "__main__":
    main()
